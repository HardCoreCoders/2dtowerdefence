package de.hcc.towerdefence.model.sub;

public class Monster extends Entity{
	private double shield;
	private double speed;
	private double lives;
	private Zone habitat;

	protected Monster(double shield, double speed, double lives, Zone habitat) {
		this.shield = shield;
		this.speed = speed;
		this.lives = lives;
		this.habitat = habitat;
	}

	protected Monster(Zone habitat) {
		this(5, 5, 5, habitat);
	}

	public double getShield() {
		return shield;
	}

	protected void setShield(double shield) {
		this.shield = shield;
	}

	public double getSpeed() {
		return speed;
	}

	protected void setSpeed(double speed) {
		this.speed = speed;
	}

	public double getLives() {
		return lives;
	}

	protected void setLives(double lives) {
		this.lives = lives;
	}

	public Zone getHabitat() {
		return habitat;
	}

	protected void setHabitat(Zone habitat) {
		this.habitat = habitat;
	}
}

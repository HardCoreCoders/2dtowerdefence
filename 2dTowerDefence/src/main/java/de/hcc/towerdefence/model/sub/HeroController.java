package de.hcc.towerdefence.model.sub;

public abstract class  HeroController {

	private Hero hero;

	
	public HeroController(Hero hero) {
		this.hero = hero;
	
	}
	
	public Hero getHero() {
		return hero;
	}

	public void setHero(Hero hero) {
		this.hero = hero;
	}
	

	
	
	
}

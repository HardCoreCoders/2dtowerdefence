package de.hcc.towerdefence.model;

import de.hcc.towerdefence.model.sub.Element;

public class FireCannonTower extends CannonTower {
	public FireCannonTower() {
		super(Element.FIRE);
		setRange(getRange() - 1);
		setForce(getForce() + 4);
		setSpeed(getSpeed() + 0);
	}

	protected FireCannonTower(double range, double force, double speed) {
		super(range, force, speed, Element.FIRE);
	}
}

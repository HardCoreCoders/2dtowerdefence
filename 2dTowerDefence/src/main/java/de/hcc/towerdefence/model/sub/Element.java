package de.hcc.towerdefence.model.sub;

public enum Element {
	NOTHING	(1.00, 1.00, 1.00),
	ICE		(1.06, 1.04, 1.02),
	LIGHT	(1.10, 1.04, 1.10),
	FIRE	(1.06, 1.10, 1.08),
	STONE	(1.10, 1.08, 1.04);

	private double range;
	private double force;
	private double speed;

	private Element(double range, double force, double speed) {
		this.range = range;
		this.force = force;
		this.speed = speed;
	}

	public double getRange() {
		return range;
	}

	public double getForce() {
		return force;
	}

	public double getSpeed() {
		return speed;
	}
}

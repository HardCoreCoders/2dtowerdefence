package de.hcc.towerdefence.model;

import de.hcc.towerdefence.model.sub.Element;
import de.hcc.towerdefence.model.sub.Tower;
import de.hcc.towerdefence.model.sub.Zone;

public class CannonTower extends Tower {

	public CannonTower() {
		super(Element.NOTHING, Zone.LAND);
		setRange(getRange() - 1);
		setForce(getForce() + 4);
		setSpeed(getSpeed() + 0);
	}

	protected CannonTower(double range, double force, double speed, Element element) {
		super(range, force, speed, element, Zone.LAND);
	}

	protected CannonTower(Element element) {
		super(element, Zone.LAND);
	}

}

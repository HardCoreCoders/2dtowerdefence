package de.hcc.towerdefence.model.sub;

import javafx.scene.Node;

public abstract class Entity<M extends Node> {

	protected M guiElement;

	public M getGuiElement() {
		return this.guiElement;
	}

	public void setGuiElement(M guiElement) {
		this.guiElement = guiElement;
	}

}

package de.hcc.towerdefence.model;

import de.hcc.towerdefence.model.sub.Element;
import de.hcc.towerdefence.model.sub.Tower;
import de.hcc.towerdefence.model.sub.Zone;

public class ArrowTower extends Tower {

	public ArrowTower() {
		super(Element.NOTHING, Zone.BOTH);
		setRange(getRange() + 2);
		setForce(getForce() - 1);
		setSpeed(getSpeed() + 2);
	}

	protected ArrowTower(double range, double force, double speed, Element element) {
		super(range, force, speed, element, Zone.BOTH);
	}

	protected ArrowTower(Element element) {
		super(element, Zone.BOTH);
	}

}

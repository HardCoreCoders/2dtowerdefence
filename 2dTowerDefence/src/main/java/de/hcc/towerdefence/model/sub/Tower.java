package de.hcc.towerdefence.model.sub;

import java.io.IOException;
import java.text.DecimalFormat;

import de.hcc.towerdefence.view.TowerModel;

public abstract class Tower extends Entity<TowerModel> {
	private double range;
	private double force;
	private double speed;
	private Element element;
	private Zone attackingZone;

	protected Tower(double range, double force, double speed, Element element, Zone attackingZone) {
		setRange(range);
		setForce(force);
		setSpeed(speed);
		setElement(element);
		setAttackingZone(attackingZone);
		setTowerModel();
	}

	protected Tower(Element element, Zone attackingZone) {
		this(5, 5, 5, element, attackingZone);
	}

	protected Tower() {
	}

	public Tower upgrade(Class<? extends Tower> c) {
		if (!this.getClass().isAssignableFrom(c))
			return null;

		try {
			return c.getConstructor().newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public double getRange() {
		return range * element.getRange();
	}

	protected void setRange(double range) {
		this.range = range;
	}

	public double getForce() {
		return force * element.getForce();
	}

	protected void setForce(double force) {
		this.force = force;
	}

	public double getSpeed() {
		return speed * element.getSpeed();
	}

	protected void setSpeed(double speed) {
		this.speed = speed;
	}

	public Element getElement() {
		return element;
	}

	protected void setElement(Element element) {
		this.element = element;
	}

	public Zone getAttackingZone() {
		return attackingZone;
	}

	protected void setAttackingZone(Zone attackingZone) {
		this.attackingZone = attackingZone;
	}

	protected void setElement(Zone attackingZone) {
		this.attackingZone = attackingZone;
	}

	public String toString() {
		DecimalFormat df = new DecimalFormat("0.00");
		
		String str = this.getClass().getSimpleName();
		str += "(Force:"+df.format(this.getForce())+";"
				+ "Range:"+df.format(this.getRange())+";"
				+ "Speed:"+df.format(this.getSpeed())+";"
				+ "Element:"+this.getElement()+";"
				+ "AttackingZone:"+this.getAttackingZone()+")";
		return str;
	}
	
	
	protected void setTowerModel(){
		try {
			guiElement = new TowerModel(this.getClass().getSimpleName());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}

package de.hcc.towerdefence.model;

import de.hcc.towerdefence.model.sub.Element;

public class FireArrowTower extends ArrowTower {
	public FireArrowTower() {
		super(Element.FIRE);
		setRange(getRange() + 2);
		setForce(getForce() - 1);
		setSpeed(getSpeed() + 2);
	}

	protected FireArrowTower(double range, double force, double speed) {
		super(range, force, speed, Element.FIRE);
	}
}

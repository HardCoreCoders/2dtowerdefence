package de.hcc.towerdefence.model.sub;

import java.io.IOException;

import de.hcc.towerdefence.view.HeroModel;

public class Hero extends Entity<HeroModel> {
	
	private String name;
	
	public Hero() {
		try {
			guiElement = new HeroModel("Basic");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public void moveTo(double x, double y){
		
	};
	public void buildTowerAt(double x, double y, Tower tower){
		
	};
	public void upgradeTower(Tower tower){
		
	};
	
	
	
}

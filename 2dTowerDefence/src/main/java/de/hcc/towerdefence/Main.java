package de.hcc.towerdefence;

import java.io.File;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import de.hcc.towerdefence.map.objects.Map;
import de.hcc.towerdefence.view.Game;
import de.hcc.towerdefence.view.TowerModel;
import de.hcc.towerdefence.view.effects.TowerCreatedEffect;

public class Main extends Application {

	public static void main(String[] args) {

		// In der IDE muss ein run Paramether hinzugefügt werden ungefähr so:
		// "-rp=F:/github/2dtowerdefence/2dTowerDefence/target/classes/"

		String path = "";
		for (String arg : args) {
			try {
				if (arg.startsWith("-ResourcesPath") || arg.startsWith("-rp")) {
					path = arg.split("=")[1];
				}
			} catch (Exception e) {
				System.err.println("Failed to parse Parameter: " + arg);
			}

		}
		try {
			ResourceHandler.initialize(path);
		} catch (Exception e) {
			e.printStackTrace();
		}

		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {

		Pane root = new Pane();

		Scene scene = new Scene(root);

		
		Map testMap = Map.getMap(new File("C:\\Users\\aa18407\\Desktop\\GreenHouse1.map"));
		System.out.println(testMap.getWaves().size());
		
		Game game = new Game("GreenHouse1");

		root.getChildren().add(game.getGamePane());

		TowerModel model = new TowerModel("FireArrowTower");
		model.buildFinished(32, 32, game.getGamePane());
		
		root.getChildren().add(	model.getNewProjecttile(true));


		stage.setScene(scene);
		stage.show();

		game.setViewPoint(50, 50);

	}

}

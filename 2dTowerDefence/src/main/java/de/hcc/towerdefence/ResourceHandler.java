package de.hcc.towerdefence;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ResourceHandler {
	
	private static String baseDir = "";
	private static JarFile jar;
	
	public static void initialize(String resourcesPath) throws Exception {
		
		if (!resourcesPath.isEmpty()) {
			baseDir = resourcesPath;
		} else {
			
			try {
				File jarFile = new File(ResourceHandler.class.getProtectionDomain().getCodeSource().getLocation().toURI()
						.getPath());
				baseDir = jarFile.getParent() + File.separator + ".." + File.separator + "resources" + File.separator;
				jar = new JarFile(jarFile);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			
		}
		
		if (baseDir.isEmpty()) {
			throw new Exception("Failed to Load Resources BaseDir");
		} else {
			System.out.println("Resource BaseDir setted to: " + baseDir);
		}
		
	}
	
	private static boolean extractFile(String resourceName) {
		
		if (jar != null) {
			resourceName = parseResourceName(resourceName);
			System.out.println("Searching for File in jar");
			for (Enumeration<JarEntry> file = jar.entries(); file.hasMoreElements();) {
				JarEntry entry = file.nextElement();
				if (!entry.getName().endsWith("class")) {
					if (entry.getName().equals(resourceName)) {
						System.out.println("File found in jar");
						File fl = new File(baseDir, entry.getName());
						if (!fl.exists()) {
							fl.getParentFile().mkdirs();
							fl = new File(baseDir, entry.getName());
						}
						if (entry.isDirectory()) {
							continue;
						}
						try {
							InputStream is = jar.getInputStream(entry);
							FileOutputStream fo = new FileOutputStream(fl);
							while (is.available() > 0) {
								fo.write(is.read());
							}
							fo.close();
							is.close();
							return true;
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		return false;
		
	}
	
	public static URL getResourceURL(String resourceName) {
		try {
			return getResource(resourceName).toURI().toURL();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static File getResource(String resourceName) {
		try {
			resourceName = parseResourceName(resourceName);
			File file = new File(baseDir + resourceName);
			return file;
		} catch (Exception e) {
			System.err.println("Failed to get Resource " + resourceName + " from baseDir " + baseDir
					+ "\nTry to extract it from jar");
			boolean extracted = extractFile(resourceName);
			if (extracted) { return getResource(resourceName); }
			
		}
		return null;
	}
	
	public static InputStream getResourceAsStream(String resourceName) {
		InputStream stream = null;
		try {
			resourceName = parseResourceName(resourceName);
			stream = new FileInputStream(baseDir + resourceName);
		} catch (FileNotFoundException e) {
			System.err.println("Failed to get Resource " + resourceName + " from baseDir " + baseDir
					+ "\nTry to extract it from jar");
			boolean extracted = extractFile(resourceName);
			if (extracted) { return getResourceAsStream(resourceName); }
		}
		return stream;
		
	}
	
	private static String parseResourceName(String resourceName) {
		// resourceName = resourceName.replaceAll("\\", "/");
		if (resourceName.startsWith("/")) {
			resourceName = resourceName.replaceFirst("/", "");
		}
		
		return resourceName;
	}

}
package de.hcc.towerdefence.util;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.Node;
import de.hcc.towerdefence.model.sub.Entity;

public class ListUtils {
	
	public static <M extends Node, T extends Entity<M>> List<M> getGuiElementsFromEntityList(List<T> entityList) {
		List<M> guiLements = new ArrayList<M>();
		for (T entity : entityList) {
			guiLements.add(entity.getGuiElement());
		}
		
		return guiLements;
	}

}

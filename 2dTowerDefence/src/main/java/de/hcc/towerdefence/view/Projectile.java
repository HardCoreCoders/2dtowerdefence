package de.hcc.towerdefence.view;

import java.awt.image.BufferedImage;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.util.Duration;

public class Projectile extends ImageView {

	private final static double timePerImage = 150;
	
	
	Image projectile1;
	Image projectile2;
	Image projectile3;
	Image projectile4;
	
	Timeline animation;
	
	public Projectile(BufferedImage bufferedImage, boolean animated) {


		projectile1 = SwingFXUtils.toFXImage(
				bufferedImage.getSubimage(0 * 32, 0, 32, 32),
				new WritableImage(32, 32));
		projectile2 = SwingFXUtils.toFXImage(
				bufferedImage.getSubimage(1 * 32, 0, 32, 32),
				new WritableImage(32, 32));
		projectile3 = SwingFXUtils.toFXImage(
				bufferedImage.getSubimage(2 * 32, 0, 32, 32),
				new WritableImage(32, 32));
		projectile4 = SwingFXUtils.toFXImage(
				bufferedImage.getSubimage(3 * 32,0, 32, 32),
				new WritableImage(32, 32));

		
		setImage(projectile1);
		
		if(animated){
			
			animation = new Timeline();
			KeyFrame frame1 = new KeyFrame(Duration.millis(timePerImage * 1), new EventHandler<ActionEvent>(){

				public void handle(ActionEvent event) {
					setImage(projectile1);
				}});
			KeyFrame frame2 =new KeyFrame(Duration.millis(timePerImage * 2), new EventHandler<ActionEvent>(){

				public void handle(ActionEvent event) {
					setImage(projectile2);
				}});
						
			KeyFrame frame3 =new KeyFrame(Duration.millis(timePerImage * 3), new EventHandler<ActionEvent>(){

				public void handle(ActionEvent event) {
					setImage(projectile3);
				}});
			
			KeyFrame frame4 =new KeyFrame(Duration.millis(timePerImage * 4), new EventHandler<ActionEvent>(){

				public void handle(ActionEvent event) {
					setImage(projectile4);
				}});
			
			animation.getKeyFrames().addAll(frame1,frame2,frame3,frame4);
			animation.setCycleCount(Animation.INDEFINITE);
			animation.play();
			
		}
		
		
	}
	
	
	public void destroy(){
		if(animation != null){
			animation.stop();
		}
	}
	
	
}

package de.hcc.towerdefence.view.effects;

import java.util.Random;

import de.hcc.towerdefence.ResourceHandler;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.image.Image;
import javafx.util.Duration;

public class TowerCreatedEffect extends Effect {
	
	private Image image = new Image(ResourceHandler.getResourceAsStream("/graphics/Effects/TowerCreated.png"));
	private Random random = new Random();;
	
	public TowerCreatedEffect (double x, double y) {
		
		int particelAmount = 10;
		
		for (int i = 1; particelAmount > i; i++) {
			Particle particle = new Particle(image);
			particle.setFitHeight(15);
			particle.setFitWidth(15);
			int toX = random.nextInt(32 + 12) - 12;
			int toY = random.nextInt(32 + 12) - 12;
			particle.setLayoutX(x - 22.5 + toX);
			particle.setLayoutY(y - 22.5 + toY);
			generadeParticelAnimation(particle);
			particles.add(particle);
		}
		
	}

	@Override
	protected void generadeParticelAnimation(Particle particle) {
		int mutlipicator = random.nextInt(2) - 1;
		int millis = random.nextInt(1500) + 500;
		Timeline particleAnimation = new Timeline();
		particleAnimation.getKeyFrames().addAll(
				new KeyFrame(Duration.millis(millis), new KeyValue(particle.fitHeightProperty(), 0)),
				new KeyFrame(Duration.millis(millis), new KeyValue(particle.fitWidthProperty(), 0)));
		

			particleAnimation.getKeyFrames().add(
					new KeyFrame(Duration.millis(millis), new KeyValue(particle.rotateProperty(), 360.0 * mutlipicator)));
		
		particle.setAnimation(particleAnimation);
	}
	
}

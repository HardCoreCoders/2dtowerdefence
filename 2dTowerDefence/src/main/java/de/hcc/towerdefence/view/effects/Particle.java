package de.hcc.towerdefence.view.effects;

import javafx.animation.Animation;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Particle extends ImageView{


	private Animation animation;
	
	public Particle(Image image) {
		setImage(image);
	}

	public Animation getAnimation() {
		return animation;
	}

	public void setAnimation(Animation animation) {
		this.animation = animation;
	}
	
}
package de.hcc.towerdefence.view.effects;

import java.util.ArrayList;
import java.util.List;

import javafx.animation.Animation;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.Pane;

public abstract class Effect {

	protected List<Particle> particles = new ArrayList<Particle>();

	public List<Particle> getParticles() {
		return particles;
	}

	public void startEffect(Pane parent) {
		parent.getChildren().addAll(particles);
		
		for (final Particle particle : particles) {
			Animation animation = particle.getAnimation();
			if (animation != null) {
				animation.setOnFinished(new EventHandler<ActionEvent>() {
					
					public void handle(ActionEvent arg0) {
						if (particle.getParent() != null) {
							Pane parent = (Pane) particle.getParent();
							parent.getChildren().remove(particle);
						}
					}
				});
				animation.play();
			}

		}
	}

	protected void removeParticles() {
		

	}
	
	protected abstract void generadeParticelAnimation(Particle particle);


}

package de.hcc.towerdefence.view;

import java.io.File;
import java.util.List;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import de.hcc.towerdefence.ResourceHandler;
import de.hcc.towerdefence.model.sub.Entity;
import de.hcc.towerdefence.util.ListUtils;
import de.kogs.javafx.tilemap.TileMap;
import de.kogs.javafx.tilemap.elements.TileSetElement;
import de.kogs.javafx.tilemap.exceptions.MapLoadException;
import de.kogs.javafx.tilemap.layers.Layer;
import de.kogs.javafx.tilemap.layers.ObjectLayer;
import de.kogs.javafx.tilemap.layers.TileLayer;

public class Game {

	private TileMap tileMap;

	/* Is used to add Object to the game. Such as Players mobs or Towers ... */
	private ObjectLayer gameLayer;

	/* Is used to find the Mobspawns */
	private TileLayer groundLayer;

	private List<TileSetElement> mobSpawns;

	private Pane gamePane;

	private ObjectProperty<Point2D> viewPoint = new SimpleObjectProperty<Point2D>();


	private ObservableList<Entity> entities = FXCollections.<Entity>observableArrayList();

	
	public Game(String name) throws MapLoadException {
		this(ResourceHandler
				.getResource("/maps/" + name + "/" + name + ".json"));
	}

	public Game(File mapFile) throws MapLoadException {
		tileMap = new TileMap(mapFile);
		tileMap.render();

		gameLayer = tileMap.getObjectLayerByName("game");
		if (gameLayer == null) {
			throw new MapLoadException("No Game ObjectLayer found");
		}

		Layer gLayer = tileMap.getLayerByName("ground");
		if (gLayer == null || gLayer.getClass() != TileLayer.class) {
			throw new MapLoadException("No Ground TileLayer found");
		}

		this.groundLayer = (TileLayer) gLayer;

		mobSpawns = groundLayer.getElementsWithPropertie("mobSpawn", null);
		if (mobSpawns.size() == 0) {
			throw new MapLoadException("No Mob Spawn found");
		}

		entities.addListener(new ListChangeListener<Entity>() {
			
			public void onChanged(ListChangeListener.Change<? extends Entity> change) {
				change.next();
				gameLayer.getChildren().addAll(ListUtils.getGuiElementsFromEntityList(change.getAddedSubList()));

				gameLayer.getChildren().removeAll(ListUtils.getGuiElementsFromEntityList(change.getRemoved()));
			}
		});

		initializGUI();

	}

	private void initializGUI() {
		gamePane = new Pane();
		gamePane.getChildren().add(tileMap);
		
		viewPoint.addListener(new ChangeListener<Point2D>() {
			
			public void changed(ObservableValue<? extends Point2D> observable, Point2D oldValue, Point2D newValue) {
				// TODO Auto-generated method stub
				
			}
		});

		// add Hub etc
	}

	public void setViewPoint(double x, double y) {
		viewPoint.set(new Point2D(x, y));
	}

	private ChangeListener<Number> viewListener;
	private Entity boundedViewEntity;

	public void bindViewPoint(Entity entity) {
		unBindViewPoint();
		boundedViewEntity = entity;
		viewListener = new ChangeListener<Number>() {

			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {
				setViewPoint(boundedViewEntity.getGuiElement().getLayoutX(),
						boundedViewEntity.getGuiElement().getLayoutY());
			}
		};
		entity.getGuiElement().layoutXProperty().addListener(viewListener);
		entity.getGuiElement().layoutYProperty().addListener(viewListener);
	}

	public void unBindViewPoint() {
		if (boundedViewEntity != null && viewListener != null) {
			boundedViewEntity.getGuiElement().layoutXProperty()
					.removeListener(viewListener);
			boundedViewEntity.getGuiElement().layoutYProperty()
					.removeListener(viewListener);
		}
	}

	public Pane getGamePane() {
		return gamePane;
	}

	public TileMap getTileMap() {
		return tileMap;
	}

	public void setTileMap(TileMap tileMap) {
		this.tileMap = tileMap;
	}

	public List<TileSetElement> getMobSpawns() {
		return mobSpawns;
	}

	public void setMobSpawns(List<TileSetElement> mobSpawns) {
		this.mobSpawns = mobSpawns;
	}

	public ObservableList<Entity> getEntities() {
		return entities;
	}







}

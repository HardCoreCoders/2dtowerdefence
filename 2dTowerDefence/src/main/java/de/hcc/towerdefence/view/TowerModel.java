package de.hcc.towerdefence.view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

import javax.imageio.ImageIO;

import de.hcc.towerdefence.ResourceHandler;
import de.hcc.towerdefence.view.effects.TowerCreatedEffect;

public class TowerModel extends Pane {

	String name;

	Image towerBase;
	Image towerBaseDestroyed;
	Image towerCannon;
	Image towerCannonDestroyed;
	
	ImageView towerCannonView;
	ImageView towerBaseView;

	private DoubleProperty angelProperty = new SimpleDoubleProperty(0);

	private BufferedImage towerBufferdI;

	public TowerModel(String name) throws IOException {
		File towerImage = ResourceHandler.getResource("graphics/Tower/" + name
				+ ".png");
		
		if (!towerImage.exists()) {
			System.err.println("TowerModel" + name + " not found");
			towerImage = ResourceHandler
					.getResource("graphics/Tower/Error.png");
		}

		towerBufferdI = ImageIO.read(towerImage);

		towerBase = SwingFXUtils.toFXImage(
				towerBufferdI.getSubimage(0 * 32, 0, 32, 32),
				new WritableImage(32, 32));
		towerBaseDestroyed = SwingFXUtils.toFXImage(
				towerBufferdI.getSubimage(1 * 32, 0, 32, 32),
				new WritableImage(32, 32));
		towerCannon = SwingFXUtils.toFXImage(
				towerBufferdI.getSubimage(2 * 32, 0, 32, 32),
				new WritableImage(32, 32));
		towerCannonDestroyed = SwingFXUtils.toFXImage(
				towerBufferdI.getSubimage(3 * 32, 0, 32, 32),
				new WritableImage(32, 32));

	
		
		
		towerBaseView = new ImageView(towerBase);
		towerCannonView = new ImageView(towerCannon);

		towerCannonView.rotateProperty().bind(angelProperty);

		setWidth(32);
		setHeight(32);
		setMaxHeight(32);
		setMaxWidth(32);
		setMinWidth(32);
		setMinHeight(32);
		setPrefHeight(32);
		setPrefWidth(32);
		
		
		getChildren().addAll(towerBaseView, towerCannonView);

		
		Timeline testAngel = new Timeline();
		testAngel.getKeyFrames().add(new KeyFrame(Duration.seconds(2), new KeyValue(angelProperty, 360)));
		testAngel.setCycleCount(-1);
		testAngel.play();
		
		setOnMouseClicked(new EventHandler<Event>() {

			public void handle(Event event) {
				destroy();
			}
			
		});
		
	}

	public void setAngel(double angel) {
		angelProperty.set(angel);
	}

	public void destroy() {
		towerBaseView.setImage(towerBaseDestroyed);
		towerCannonView.setImage(towerCannonDestroyed);
	}
	
	public void buildFinished(double x, double y, Pane parent) {
		TowerCreatedEffect createdEffect = new TowerCreatedEffect(x + 32, y + 32);
		createdEffect.startEffect(parent);
		this.setLayoutX(x);
		this.setLayoutY(y);
		parent.getChildren().add(this);
	}
	
	public Node getNewProjecttile(boolean animated){
		Projectile projecttile = new Projectile(towerBufferdI.getSubimage(0, 32, 4*32, 32),animated);
		projecttile.setRotate(angelProperty.get());
		return projecttile;
	}
	
}

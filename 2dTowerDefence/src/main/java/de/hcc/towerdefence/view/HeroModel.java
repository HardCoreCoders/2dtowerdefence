package de.hcc.towerdefence.view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;

import de.hcc.towerdefence.ResourceHandler;

public class HeroModel extends ImageView {

	private BufferedImage heroBufferdI;
	
	private Image standStill;
	private Image walk;
	private Image build;
	

	public HeroModel (String name) throws IOException {
		File heroImage = ResourceHandler.getResource("graphics/Hero/" + name + ".png");
		
		if (!heroImage.exists()) {
			System.err.println("TowerModel" + name + " not found");
			heroImage = ResourceHandler.getResource("graphics/Tower/Error.png");
		}
		
		heroBufferdI = ImageIO.read(heroImage);
		
		standStill = SwingFXUtils.toFXImage(heroBufferdI.getSubimage(0 * 32, 0, 32, 32), new WritableImage(32, 32));

		setImage(standStill);
	}

	

}

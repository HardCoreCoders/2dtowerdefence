package de.hcc.towerdefence;

import de.hcc.towerdefence.model.ArrowTower;
import de.hcc.towerdefence.model.CannonTower;
import de.hcc.towerdefence.model.FireArrowTower;
import de.hcc.towerdefence.model.FireCannonTower;
import de.hcc.towerdefence.model.FreezeTower;
import de.hcc.towerdefence.model.sub.Tower;

public class Test {
	public static void main(String[] args) {
		Tower t1;
		t1 = new ArrowTower();System.out.println(t1);
		t1 = t1.upgrade(FireArrowTower.class);System.out.println(t1);
		
		System.out.println();
		
		t1 = new CannonTower();System.out.println(t1);
		t1 = new FireCannonTower();System.out.println(t1);
		
		System.out.println();
		
		t1 = new FreezeTower();System.out.println(t1);
	}
}

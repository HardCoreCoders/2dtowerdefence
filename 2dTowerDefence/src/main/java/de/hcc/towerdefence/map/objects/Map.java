package de.hcc.towerdefence.map.objects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.zeroturnaround.zip.ZipUtil;

import de.hcc.towerdefence.model.sub.Zone;
import de.hcc.towerdefence.view.Game;
import de.kogs.javafx.tilemap.TileMap;
import de.kogs.javafx.tilemap.exceptions.MapLoadException;

public class Map {

	private String name;
	private File tileMapJsonFile;
	private ObservableList<Wave> waves = FXCollections.observableArrayList();

	public Map() {
	}

	public static Map getMap(File zippedMap) throws IOException, ParseException {
		if (zippedMap.isFile()
				&& zippedMap.getAbsoluteFile().toString().endsWith(".map")) {
			File tempFolder = new File(zippedMap.getParentFile()
					.getAbsolutePath() + "/temp");
			ZipUtil.unpack(new FileInputStream(zippedMap), tempFolder);
			System.out.println("UNpacket");

			File mapJsonFile = new File(tempFolder.getAbsolutePath()
					+ "/map.json");
			JSONObject mapJson = (JSONObject) (new JSONParser())
					.parse(new FileReader(mapJsonFile));
			System.out.println(mapJson);
			return 	Map.getMapFromJSON(mapJson);
		}
		return null;
	}

	private Game lastTestedGame;

	public boolean testMap() {
		if (tileMapJsonFile != null) {

			try {
				setLastTestedGame(new Game(tileMapJsonFile));
				return true;
			} catch (MapLoadException e) {
				return false;
			}

		}
		return false;

	}

	public JSONObject getMapAsJSONObject() {
		JSONObject object = new JSONObject();
		object.put("name", name);

		JSONArray jsonWaves = new JSONArray();
		for (Wave wave : waves) {
			jsonWaves.add(wave.getWaveAsJSONObject());
		}

		object.put("waves", jsonWaves);

		return object;
	}

	public static Map getMapFromJSON(JSONObject mapJson) {
		Map map = new Map();

		map.setName((String) mapJson.get("name"));
		JSONArray wavesJson = (JSONArray) mapJson.get("waves");
		for (Object object : wavesJson) {

			JSONObject jsonObject = (JSONObject) object;
			map.getWaves().add(Wave.getWaveFromJsonObject(jsonObject));

		}

		return map;
	}

	public File getTileMapJsonFile() {
		return tileMapJsonFile;
	}

	public void setTileMapJsonFile(File tileMapJsonFile) {
		this.tileMapJsonFile = tileMapJsonFile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Game getLastTestedGame() {
		return lastTestedGame;
	}

	public void setLastTestedGame(Game lastTestedGame) {
		this.lastTestedGame = lastTestedGame;
	}

	public ObservableList<Wave> getWaves() {
		return waves;
	}

	public void setWaves(ObservableList<Wave> waves) {
		this.waves = waves;
	}

	public void copyNeededFilesTo(File file) {
		if (file.isDirectory()) {
			try {
				FileUtils.copyFileToDirectory(tileMapJsonFile, file);

				TileMap tileMap = new TileMap(tileMapJsonFile);
				for (File tileSetFile : tileMap.getTileSetFiles()) {
					FileUtils.copyFileToDirectory(tileSetFile,
							new File(file.getAbsolutePath()));
				}
				File jsonMapFile = new File(file.getAbsoluteFile()
						+ "/map.json");
				FileWriter filewriter = new FileWriter(jsonMapFile);
				filewriter.write(getMapAsJSONObject().toJSONString());
				filewriter.flush();
				filewriter.close();

			} catch (IOException e) {

			} catch (MapLoadException e) {
				e.printStackTrace();
			}
		}
	}

}

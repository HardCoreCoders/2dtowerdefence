package de.hcc.towerdefence.map.objects;

import org.json.simple.JSONObject;

public class Wave implements Comparable<Wave>{
	
	private int number = 0;
	private MobType mobType;
	private int mobAmount;
	
	public Wave (int number, MobType mobType, int mobAmount) {
		this.mobAmount = mobAmount;
		this.setNumber(number);
		this.setMobType(mobType);
	}
	
	public int getNumber() {
		return number;
	}
	
	public void setNumber(int number) {
		this.number = number;
	}

	public MobType getMobType() {
		return mobType;
	}
	
	public void setMobType(MobType mobType) {
		this.mobType = mobType;
	}
	
	public int getMobAmount() {
		return mobAmount;
	}

	public void setMobAmount(int mobAmount) {
		this.mobAmount = mobAmount;
	}

	public JSONObject getWaveAsJSONObject() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("number", number);
		jsonObject.put("MobType", mobType.getMobTypeAsJSONObject());
		jsonObject.put("mobAmount", mobAmount);
		return jsonObject;
	}
	public static Wave getWaveFromJsonObject(JSONObject jsonObject){

		
		int number = ((Long) jsonObject.get("number")).intValue();
		int mobAmount = ((Long) jsonObject.get("mobAmount")).intValue();

		JSONObject mobTypeJson = (JSONObject) jsonObject.get("MobType");
		
		return new Wave(number, MobType.getMobTypeFromJSONObject(mobTypeJson) , mobAmount);
	}
	
	
	public int compareTo(Wave o) {
		return Integer.compare(getNumber(), o.getNumber());
	}

}
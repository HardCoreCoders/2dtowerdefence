package de.hcc.towerdefence.map.objects;

import org.json.simple.JSONObject;

import de.hcc.towerdefence.model.sub.Zone;


public class MobType {
	
	private double shield;
	private double speed;
	private double lives;
	private Zone habitat;
	private MobModell modell;
	
	public MobType (double shield, double speed, double lives, Zone habitat, MobModell modell) {
		super();
		this.shield = shield;
		this.speed = speed;
		this.lives = lives;
		this.habitat = habitat;
		this.modell = modell;

	}
	
	public JSONObject getMobTypeAsJSONObject() {
		JSONObject jsonObject = new JSONObject();
		
		jsonObject.put("shield", shield);
		jsonObject.put("speed", speed);
		jsonObject.put("lives", lives);
		jsonObject.put("habitat", habitat +"");
		jsonObject.put("modell", modell+ "");
		return jsonObject;
	}

	public static MobType getMobTypeFromJSONObject(JSONObject json){
		
	
		MobType mobType = new MobType((Double)json.get("shield"), (Double)json.get("speed"), (Double)json.get("lives"), Zone.valueOf((String)json.get("habitat")), MobModell.valueOf((String)json.get("modell")));
		
		return mobType;
	}
	

	public double getShield() {
		return shield;
	}
	
	public double getSpeed() {
		return speed;
	}
	
	public double getLives() {
		return lives;
	}
	
	public Zone getHabitat() {
		return habitat;
	}
	
	
	public void setShield(double shield) {
		this.shield = shield;
	}
	
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	public void setLives(double lives) {
		this.lives = lives;
	}
	
	public void setHabitat(Zone habitat) {
		this.habitat = habitat;
	}

	public MobModell getModell() {
		return modell;
	}

	public void setModell(MobModell modell) {
		this.modell = modell;
	}
	
	@Override
	public String toString() {
		return "MobType [shield=" + shield + ", speed=" + speed + ", lives=" + lives + ", habitat=" + habitat + ", modell="
				+ modell + "]";
	}

	

}

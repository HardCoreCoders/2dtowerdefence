package de.hcc.towerdefence.map.creator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.text.html.HTMLDocument.HTMLReader.PreAction;

import org.apache.commons.io.FileUtils;
import org.json.simple.parser.JSONParser;
import org.zeroturnaround.zip.ZipUtil;

import de.hcc.towerdefence.map.components.DoubleTableCell;
import de.hcc.towerdefence.map.components.FileTextField;
import de.hcc.towerdefence.map.components.WaveNumberTableCell;
import de.hcc.towerdefence.map.objects.Map;
import de.hcc.towerdefence.map.objects.MobModell;
import de.hcc.towerdefence.map.objects.MobType;
import de.hcc.towerdefence.map.objects.Wave;
import de.hcc.towerdefence.model.sub.Zone;
import de.hcc.towerdefence.view.Game;
import de.kogs.javafx.tilemap.TileMap;
import de.kogs.javafx.tilemap.TileSet;
import de.kogs.javafx.tilemap.layers.Layer;
import de.kogs.javafx.tilemap.layers.ObjectLayer;
import de.kogs.javafx.tilemap.layers.TileLayer;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Popup;
import javafx.util.Callback;

public class MapCreatorController implements Initializable {
	
	@FXML
	FileTextField jsonMapFile;

	@FXML
	Accordion mapEditing;
	@FXML
	HBox actions;
	
	@FXML
	TextField name;

	@FXML
	Label mapsize;

	@FXML
	Label layersInfo;

	@FXML
	Label tilesetsInfo;

	@FXML
	Label spawnInfo;
	
	@FXML
	TableView<MobType> mobsTable;

	@FXML
	TableView<Wave> wavesTable;

	Map map;

	public void initialize(URL location, ResourceBundle resources) {

		jsonMapFile.fileProperty.addListener(new ChangeListener<File>() {
			
			public void changed(ObservableValue<? extends File> observable, File oldValue, File newValue) {
				map.setTileMapJsonFile(newValue);
				name.setText(newValue.getName().replace(".json", ""));
				testMap();
			}
		});
		
		name.textProperty().addListener(new ChangeListener<String>() {
			
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				map.setName(newValue);
			}
		});

		initializeMobsTable();
		initializeWavesTable();
	}
	

	private void initializeWavesTable() {
		wavesTable.setEditable(true);
		
		TableColumn<Wave, Integer> numberCol = new TableColumn<Wave, Integer>("Wave Number");
		numberCol.setCellValueFactory(new PropertyValueFactory<Wave, Integer>("number"));
		numberCol.setCellFactory(new Callback<TableColumn<Wave, Integer>, TableCell<Wave, Integer>>() {
			
			public TableCell<Wave, Integer> call(TableColumn<Wave, Integer> param) {
				return new WaveNumberTableCell(wavesTable.getItems());
			}
		});

		TableColumn<Wave, MobType> mobType = new TableColumn<Wave, MobType>("MobType");
		mobType.setCellValueFactory(new PropertyValueFactory<Wave, MobType>("mobType"));
		mobType.setCellFactory(new Callback<TableColumn<Wave, MobType>, TableCell<Wave, MobType>>() {
			
			public TableCell<Wave, MobType> call(TableColumn<Wave, MobType> param) {
				return new ComboBoxTableCell<Wave, MobType>(mobsTable.getItems());
			}
		});

		wavesTable.getColumns().addAll(numberCol, mobType);
	}

	private void initializeMobsTable() {
		mobsTable.setEditable(true);

		TableColumn<MobType, MobModell> modellNameCol = new TableColumn<MobType, MobModell>("ModellName");
		modellNameCol.setCellValueFactory(new PropertyValueFactory<MobType, MobModell>("modell"));
		modellNameCol.setCellFactory(new Callback<TableColumn<MobType, MobModell>, TableCell<MobType, MobModell>>() {
			
			public TableCell<MobType, MobModell> call(TableColumn<MobType, MobModell> param) {
				ObservableList<MobModell> modells = FXCollections.<MobModell>observableArrayList();
				modells.addAll(MobModell.values());
				return new ComboBoxTableCell<MobType, MobModell>(modells);
			}
		});
		
		TableColumn<MobType, Double> shieldCol = new TableColumn<MobType, Double>("Shield");
		shieldCol.setCellValueFactory(new PropertyValueFactory<MobType, Double>("shield"));
		shieldCol.setCellFactory(new Callback<TableColumn<MobType, Double>, TableCell<MobType, Double>>() {
			
			public TableCell<MobType, Double> call(TableColumn<MobType, Double> param) {
				return new DoubleTableCell<MobType>(0, 100, 50);
			}
		});
		shieldCol.setEditable(true);
		
		TableColumn<MobType, Double> speed = new TableColumn<MobType, Double>("Speed");
		speed.setCellValueFactory(new PropertyValueFactory<MobType, Double>("speed"));
		speed.setCellFactory(new Callback<TableColumn<MobType, Double>, TableCell<MobType, Double>>() {
			
			public TableCell<MobType, Double> call(TableColumn<MobType, Double> param) {
				return new DoubleTableCell<MobType>(0, 100, 50);
			}
		});
		speed.setEditable(true);
		
		TableColumn<MobType, Double> lives = new TableColumn<MobType, Double>("Lives");
		lives.setCellValueFactory(new PropertyValueFactory<MobType, Double>("lives"));
		lives.setCellFactory(new Callback<TableColumn<MobType, Double>, TableCell<MobType, Double>>() {
			
			public TableCell<MobType, Double> call(TableColumn<MobType, Double> param) {
				return new DoubleTableCell<MobType>(0, 1000, 50);
			}
		});
		lives.setEditable(true);
		
		TableColumn<MobType, Zone> habitat = new TableColumn<MobType, Zone>("Habitat");
		habitat.setCellValueFactory(new PropertyValueFactory<MobType, Zone>("habitat"));
		habitat.setCellFactory(new Callback<TableColumn<MobType,Zone>, TableCell<MobType,Zone>>() {

			public TableCell<MobType, Zone> call(TableColumn<MobType, Zone> param) {
				return new ComboBoxTableCell<MobType, Zone>(Zone.AIR, Zone.LAND);
			}});
		habitat.setEditable(true);

		mobsTable.getColumns().addAll(modellNameCol, shieldCol, speed, lives, habitat);
		
	}

	/**
	 * GLOBAL
	 */
	@FXML
	private void newMap() {
		map = new Map();
		mapEditing.setDisable(false);
		actions.setDisable(false);
		wavesTable.setItems(map.getWaves());
	}
	
	@FXML
	private void openMap() {
		
		mapEditing.setDisable(false);
		actions.setDisable(false);
	}
	
	@FXML
	private void searchJsonMap() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Search TileMap json File");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("*.json", "*.json"));
		File file = fileChooser.showOpenDialog(jsonMapFile.getScene().getWindow());
		if (file != null) {
			jsonMapFile.setText(file.getAbsolutePath());
		}
	}

	@FXML
	private void saveAs() {
		System.out.println(mobsTable.getItems());

		FileChooser saveDir = new FileChooser();
		saveDir.setTitle("Save Maps as");
		saveDir.getExtensionFilters().add(new ExtensionFilter("Map File", "*.map"));
		saveDir.setInitialFileName(map.getName() + "");
		File file = saveDir.showSaveDialog(jsonMapFile.getScene().getWindow());

		if (file != null) {
			
			File parentTempFolder = new File(file.getParent() + "/temp");
			parentTempFolder.mkdir();
			
			map.copyNeededFilesTo(parentTempFolder);
			System.out.println(file.getAbsolutePath());
			
			ZipUtil.pack(parentTempFolder, file);
			
			try {
				FileUtils.deleteDirectory(parentTempFolder);
			} catch (IOException e) {
				
				e.printStackTrace();
			}

		}
		
	}
	

	@FXML
	private void testMap() {
		map.testMap();
		Game game = map.getLastTestedGame();
		if (game != null) {
			TileMap tileMap = game.getTileMap();
			
			mapsize.setText(tileMap.getWidth() + " x " + tileMap.getHeight() + " px");

			int objectLayers = 0;
			int tileLayers = 0;
			for (Layer layer : tileMap.getLayers()) {
				if (layer instanceof ObjectLayer) {
					objectLayers++;
				} else if (layer instanceof TileLayer) {
					tileLayers++;
				}
			}

			layersInfo.setText("Layers: " + (objectLayers + tileLayers) + " (TileLayers: " + tileLayers + " ObjectLayers: "
					+ objectLayers + ")");
			
			tilesetsInfo.setText("TileSets: " + tileMap.getTileSets().size() + " with Elements: "
					+ TileSet.getAllElements().size());
			
			spawnInfo.setText("MobSpawns " + game.getMobSpawns().size());
		}
	}

	/**
	 * MOBS
	 */
	@FXML
	private void addMobFEMM() {
		mobsTable.getItems().add(new MobType(10, 10, 10, Zone.LAND, MobModell.RUNNER1));
	}
	
	@FXML
	private void removeMob() {
		if (mobsTable.getSelectionModel().getSelectedItem() != null) {
			mobsTable.getItems().removeAll(mobsTable.getSelectionModel().getSelectedItem());
		}
	}
	
	@FXML
	private void createWave() {
		wavesTable.getItems().add(new Wave(wavesTable.getItems().size() + 1, mobsTable.getItems().get(0), 10));
	}
	
	@FXML
	private void deleteWave() {
		
	}

}

package de.hcc.towerdefence.map.creator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class MapCreator extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		Pane root = FXMLLoader.load(MapCreator.class.getResource("/fxml/creator.fxml"));
		
		Scene scene = new Scene(root);
		scene.getStylesheets().add("/fxml/style.css");
		primaryStage.setTitle("MapCreator");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
}

package de.hcc.towerdefence.map.components;

import de.hcc.towerdefence.map.objects.Wave;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Slider;
import javafx.scene.control.TableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;


public class WaveNumberTableCell extends TableCell<Wave, Integer> {
	
	HBox graphic = new HBox();
	Button down;
	Button up;
	private ObservableList<Wave> observableList;
	
	public WaveNumberTableCell (ObservableList<Wave> observableList) {
		this.observableList = observableList;
		down = new Button("↓");
		up = new Button("↑");
		HBox.setHgrow(down, Priority.ALWAYS);
		HBox.setHgrow(up, Priority.ALWAYS);
		
		down.setOnAction(new EventHandler<ActionEvent>() {
			
			public void handle(ActionEvent event) {
				commitEdit(getItem() + 1);
			}
		});
		up.setOnAction(new EventHandler<ActionEvent>() {
			
			public void handle(ActionEvent event) {
				commitEdit(getItem() - 1);
			}
		});
		
		graphic.getChildren().addAll(down, up);
		
		setGraphic(graphic);
		setContentDisplay(ContentDisplay.TEXT_ONLY);
	}
	
	
	@Override
	public void commitEdit(Integer newValue) {
		
		for(Wave wave: observableList){
			if(wave.getNumber() == newValue){
				wave.setNumber(getItem());
				break;
			}
		}
		super.commitEdit(newValue);
		getTableView().sort();
	}
	

	@Override
	public void startEdit() {
		super.startEdit();
		setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
	}
	

	@Override
	protected void updateItem(Integer item, boolean empty) {
		super.updateItem(item, empty);
		if (!empty) {
			setText(item + "");
		} else {
			setText("");
		}
	}
	
	

	@Override
	public void cancelEdit() {
		super.cancelEdit();
		setContentDisplay(ContentDisplay.TEXT_ONLY);
	}

}

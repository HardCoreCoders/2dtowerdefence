package de.hcc.towerdefence.map.components;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.css.PseudoClass;
import javafx.scene.control.TextField;

public class FileTextField extends TextField {
	
	
	private static final PseudoClass PSEUDO_CLASS_PARSED_FAILED = PseudoClass.getPseudoClass("parsedFail");

	private Timer timer;


	public ObjectProperty<File> fileProperty = new SimpleObjectProperty<File>();

	public FileTextField () {
		
		pseudoClassStateChanged(PSEUDO_CLASS_PARSED_FAILED, false);
		
		textProperty().addListener(new ChangeListener<String>() {
			
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (timer != null) {
					timer.cancel();
				}
				timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						Platform.runLater(new Runnable() {
							public void run() {
								tryParse();
							}

						});
					}
				}, 500);
			}
		});
		
	}
	
	private void tryParse() {
		File file = new File(getText());
		if (file.isFile() && file.exists()) {
			this.setFile(file);
			pseudoClassStateChanged(PSEUDO_CLASS_PARSED_FAILED, false);
		} else {
			this.setFile(null);
			pseudoClassStateChanged(PSEUDO_CLASS_PARSED_FAILED, true);
		}
	}

	public File getFile() {
		return fileProperty.get();
	}
	
	public void setFile(File file) {
		fileProperty.set(file);
	}

}

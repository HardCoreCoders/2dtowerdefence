package de.hcc.towerdefence.map.components;


import java.text.DecimalFormat;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Slider;
import javafx.scene.control.TableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.util.StringConverter;

public class DoubleTableCell<S> extends TableCell<S, Double> {
	HBox graphic = new HBox();
	Slider slider;
	Button commit;
	
	public DoubleTableCell (double min, double max, double defaultValue) {
		slider = new Slider(min, max, defaultValue);
		slider.setMajorTickUnit(max / 10);
		slider.setShowTickMarks(true);
		slider.setShowTickLabels(true);
		HBox.setHgrow(slider, Priority.ALWAYS);
		commit = new Button();
		slider.valueProperty().addListener(new ChangeListener<Number>() {
			
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				commit.setText(((double) Math.round(newValue.doubleValue() * 100) / 100) + "");
			}
		});

		commit.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent event) {
				commitEdit(slider.getValue());
			}});
		
		graphic.getChildren().addAll(slider, commit);

		setGraphic(graphic);
		setContentDisplay(ContentDisplay.TEXT_ONLY);
	}


	@Override
	public void startEdit() {
		slider.setValue(getItem());
		super.startEdit();
		setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
	}
	
	@Override
	public void commitEdit(Double newValue) {
		super.commitEdit((double) Math.round(newValue.doubleValue() * 100) / 100);
		setContentDisplay(ContentDisplay.TEXT_ONLY);
		setItem((double) Math.round(newValue.doubleValue() * 1000) / 1000);
	}

	@Override
	protected void updateItem(Double item, boolean empty) {
		super.updateItem(item, empty);
		if (!empty) {
			setText(item.toString());
			slider.setValue(item);
		} else {
			setText("");
		}
	}
	
	@Override
	public void cancelEdit() {
		super.cancelEdit();
		slider.setValue(itemProperty().get());
		setContentDisplay(ContentDisplay.TEXT_ONLY);

	}

}

